﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using EventManagement.Models;
using EventManagement.Services;
using System.Configuration;

namespace EventManagement
{
    /// <summary>
    /// Summary description for web_service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService()]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class web_service : System.Web.Services.WebService
    {
        #region user 
        [WebMethod(Description = "Register New User")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void registerUser(string name, string phone, string email, string password, string address)
        {
            if (true)
            {
                UserModel checkemail = new UserModel();
                checkemail = DbService.getUserByEmail(email);
                if(checkemail.id == 0)
                {
                    UserModel user = new UserModel();
                    user.account = GeneralService.GenerateGUID();
                    user.name = name;
                    user.phone = phone;
                    user.email = email;
                    user.password = password;
                    user.address = address;
                    user.regTime = GeneralService.GetUTCDateTime();
                    int result = DbService.insertUser(user);
                    if (result > 0)
                    {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.Write("{\"success\":true}");
                    }
                    else
                    {
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.Write("{\"success\":false,\"message\":\"Register failed\"}");
                    }
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write("{\"success\":false,\"message\":\"Email already exist\"}");
                }
            }
            else
            {
                //return "Authentication failed.";
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write("{\"error\":\"Authentication failed\"}");
            }
        }

        [WebMethod(Description = "Login User")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void loginUser(string email, string password)
        {
            if (true)
            {
                UserModel user = new UserModel();
                user = DbService.getUserByEmail(email);
                if (user.id > 0)
                {
                    if(user.password == password)
                    {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        string retJSON = js.Serialize(user);
                        retJSON = GeneralService.FineTuneJSON(retJSON);
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.Write(retJSON);
                    }
                    else
                    {
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.Write("{\"success\":false,\"message\":\"Invalid password\"}");
                    }
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write("{\"success\":false,\"message\":\"Email not register\"}");
                }
            }
            else
            {
                //return "Authentication failed.";
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write("{\"error\":\"Authentication failed\"}");
            }
        }

        [WebMethod(Description = "Update User Profile")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void updateUserProfile(string account, string name, string phone, string email, string address)
        {
            if (true)
            {
                UserModel user = new UserModel();
                user = DbService.getUserByAccount(account);
                if (user.id > 0)
                {
                    user.name = name;
                    user.phone = phone;
                    user.email = email;
                    user.address = address;
                    int result = DbService.updateUser(user);
                    if (result > 0)
                    {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.Write("{\"success\":true}");
                    }
                    else
                    {
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.Write("{\"success\":false,\"message\":\"Update failed\"}");
                    }
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write("{\"success\":false,\"message\":\"User does not exist\"}");
                }
            }
            else
            {
                //return "Authentication failed.";
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write("{\"error\":\"Authentication failed\"}");
            }
        }

        [WebMethod(Description = "Change Password")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void changePassword(string account, string oldpassword, string newpassword)
        {
            if (true)
            {
                UserModel user = new UserModel();
                user = DbService.getUserByAccount(account);
                if (user.id > 0)
                {
                    if(user.password == oldpassword)
                    {
                        user.password = newpassword;
                        int result = DbService.updateUser(user);
                        if (result > 0)
                        {
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            this.Context.Response.ContentType = "application/json; charset=utf-8";
                            this.Context.Response.Write("{\"success\":true}");
                        }
                        else
                        {
                            this.Context.Response.ContentType = "application/json; charset=utf-8";
                            this.Context.Response.Write("{\"success\":false,\"message\":\"Change password failed\"}");
                        }
                    }
                    else
                    {
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.Write("{\"success\":false,\"message\":\"Invalid old password \"}");
                    }
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write("{\"success\":false,\"message\":\"User not exist\"}");
                }
            }
            else
            {
                //return "Authentication failed.";
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write("{\"error\":\"Authentication failed\"}");
            }
        }

        [WebMethod(Description = "Reset Password")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void resetPassword(string email)
        {
            if (true)
            {
                UserModel user = new UserModel();
                user = DbService.getUserByEmail(email);
                if (user.id > 0)
                {
                    EmailService.sendResetPassword(user);
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write("{\"success\":true}");
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write("{\"success\":false,\"message\":\"Email not register\"}");
                }
            }
            else
            {
                //return "Authentication failed.";
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write("{\"error\":\"Authentication failed\"}");
            }
        }

        [WebMethod(Description = "Get User By Id")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getUserById(int id)
        {
            if (true)
            {
                UserModel user = DbService.getUserById(id);
                if (user.id > 0)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    string retJSON = js.Serialize(user);
                    retJSON = GeneralService.FineTuneJSON(retJSON);
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(retJSON);
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write("{\"success\":false,\"message\":\"User not exist\"}");
                }
            }
            else
            {
                //return "Authentication failed.";
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write("{\"error\":\"Authentication failed\"}");
            }
        }

        [WebMethod(Description = "Get User By Account")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getUserByAccount(string account)
        {
            if (true)
            {
                UserModel user = DbService.getUserByAccount(account);
                if (user.id > 0)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    string retJSON = js.Serialize(user);
                    retJSON = GeneralService.FineTuneJSON(retJSON);
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(retJSON);
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write("{\"success\":false,\"message\":\"User not exist\"}");
                }
            }
            else
            {
                //return "Authentication failed.";
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write("{\"error\":\"Authentication failed\"}");
            }
        }

        [WebMethod(Description = "Update user profle picture")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void updateUserProfilePicture()
        {
            HttpContext context = HttpContext.Current;
            //string authentication = context.Request["authentication"];
            if (true)
            {
                try
                {
                    string account = context.Request["account"];

                    //File Collection that was submitted with posted data
                    HttpFileCollection filesPosted = context.Request.Files;
                    String filePath = filesPosted[0].FileName;

                    if (filesPosted.Count <= 0)
                        throw new ArgumentException("E12");

                    //if(!ImageHelper.CheckValidFormat(filePath))
                    //    throw new ArgumentException("E7");

                    //String ext = ImageHelper.GetExtensionName(filePath);
                    //if (ext == "")
                    //    ext = ".mp4";


                    UserModel user = DbService.getUserByAccount(account);
                    if (user.id > 0)
                    {
                        string fileName = GeneralService.GetUTCDateTime().ToString("yyyyMMddHHmmss") + ".jpg";
                        filePath = System.IO.Path.Combine(HttpContext.Current.Server.MapPath("~/images/user"), fileName);
                        filesPosted[0].SaveAs(filePath);

                        System.Drawing.Image source = System.Drawing.Image.FromFile(filePath);
                        ImageService.resizeImage(source, "images/user/", fileName);
                        source.Dispose();

                        user.picHeadUrl = ConfigurationManager.AppSettings["ProfilePicturePath"] + fileName;
                        int result = DbService.updateUser(user);

                        if (result > 0)
                        {
                            // Return JSON data
                            this.Context.Response.ContentType = "application/json; charset=utf-8";
                            this.Context.Response.Write("{\"success\":true}");
                        }
                        else
                        {
                            this.Context.Response.ContentType = "application/json; charset=utf-8";
                            this.Context.Response.Write("{\"success\":false,\"message\":\"Update failed \"}");
                        }
                    }
                    else
                    {
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.Write("{\"success\":false,\"message\":\"User not exist\"}");
                    }
                }
                catch(Exception ex)
                {
                    string message = "Error : " + ex.ToString();
                    LogService.CreateErrorLog(message, LogService.ImageService);

                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write("{\"success\":false,\"message\":\"Exception caught \"}");
                }
            }
            else
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write("{\"error\":\"Authentication failed\"}");
            }
        }
        #endregion

        #region event
        [WebMethod(Description = "Get Event By Id")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getEventById(int id)
        {
            if (true)
            {
                EventModel item = DbService.getEventById(id);
                if (item.id > 0)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    string retJSON = js.Serialize(item);
                    retJSON = GeneralService.FineTuneJSON(retJSON);
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(retJSON);
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write("{\"success\":false,\"message\":\"Event not exist\"}");
                }
            }
            else
            {
                //return "Authentication failed.";
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write("{\"error\":\"Authentication failed\"}");
            }
        }

        [WebMethod(Description = "Get Event List")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getEventList()
        {
            if (true)
            {
                List< EventModel> item = DbService.getEventList();
                if (item.Count > 0)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    string retJSON = js.Serialize(item);
                    retJSON = GeneralService.FineTuneJSON(retJSON);
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(retJSON);
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write("{\"success\":false,\"message\":\"No event found\"}");
                }
            }
            else
            {
                //return "Authentication failed.";
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write("{\"error\":\"Authentication failed\"}");
            }
        }

        [WebMethod(Description = "Get Event Content")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getEventContent(int eventid)
        {
            if (true)
            {
                List<EventContent> item = DbService.getEventContentById(eventid);
                if (item.Count > 0)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    string retJSON = js.Serialize(item);
                    retJSON = GeneralService.FineTuneJSON(retJSON);
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(retJSON);
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write("{\"success\":false,\"message\":\"No event content found\"}");
                }
            }
            else
            {
                //return "Authentication failed.";
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write("{\"error\":\"Authentication failed\"}");
            }
        }

        [WebMethod(Description = "RegisterEvent")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void registerEvent(string account, int eventid)
        {
            if (true)
            {
                UserModel user = DbService.getUserByAccount(account);
                if(user.id > 0)
                {
                    EventParticipant eventparticipant = new EventParticipant();
                    eventparticipant.eventId = eventid;
                    eventparticipant.accountId = user.id;
                    eventparticipant.isAttend = 0;
                    eventparticipant.attendDate = GeneralService.GetUTCDateTime();
                    eventparticipant.createdDate = GeneralService.GetUTCDateTime();
                    eventparticipant.createdBy = user.name;
                    eventparticipant.modifiedDate = GeneralService.GetUTCDateTime();

                    int result = DbService.insertEventParticipant(eventparticipant);
                    if (result > 0)
                    {
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.Write("{\"success\":true}");
                    }
                    else
                    {
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.Write("{\"success\":false,\"message\":\"Register failed\"}");
                    }
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write("{\"success\":false,\"message\":\"User does not exist\"}");
                }
            }
            else
            {
                //return "Authentication failed.";
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write("{\"error\":\"Authentication failed\"}");
            }
        }

        [WebMethod(Description = "Get Event List By Filter (0-all,1-upcoming,2-registered,3-attended)")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getEventListByFilter(string account, int filter, int pageIndex, int pageCount)
        {
            if (true)
            {
                List<EventModel> item = DbService.getEventListByFilter(account, filter, pageIndex, pageCount);
                if (item.Count > 0)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    string retJSON = js.Serialize(item);
                    retJSON = GeneralService.FineTuneJSON(retJSON);
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(retJSON);
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write("{\"success\":false,\"message\":\"No event found\"}");
                }
            }
            else
            {
                //return "Authentication failed.";
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write("{\"error\":\"Authentication failed\"}");
            }
        }
        #endregion
    }
}
