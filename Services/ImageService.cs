﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagement.Services
{
    public class ImageService
    {
        private static System.Drawing.Image CreateThumbnail(System.Drawing.Image imgPhoto, int Width, int Height, bool needToFill)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);
            if (!needToFill)
            {
                if (nPercentH < nPercentW)
                {
                    nPercent = nPercentH;
                }
                else
                {
                    nPercent = nPercentW;
                }
            }
            else
            {
                if (nPercentH > nPercentW)
                {
                    nPercent = nPercentH;
                    destX = (int)Math.Round((Width -
                        (sourceWidth * nPercent)) / 2);
                }
                else
                {
                    nPercent = nPercentW;
                    destY = (int)Math.Round((Height -
                        (sourceHeight * nPercent)) / 2);
                }
            }

            if (nPercent > 1)
                nPercent = 1;

            int destWidth = (int)Math.Round(sourceWidth * nPercent);
            int destHeight = (int)Math.Round(sourceHeight * nPercent);

            System.Drawing.Bitmap bmPhoto = new System.Drawing.Bitmap(
                destWidth <= Width ? destWidth : Width,
                destHeight < Height ? destHeight : Height,
                              System.Drawing.Imaging.PixelFormat.Format32bppRgb);
            //bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
            //                 imgPhoto.VerticalResolution);

            System.Drawing.Graphics grPhoto = System.Drawing.Graphics.FromImage(bmPhoto);
            grPhoto.Clear(System.Drawing.Color.White);
            grPhoto.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            //InterpolationMode.HighQualityBicubic;
            grPhoto.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            grPhoto.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            grPhoto.DrawImage(imgPhoto,
                new System.Drawing.Rectangle(destX, destY, destWidth, destHeight),
                new System.Drawing.Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                System.Drawing.GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        private static Boolean CheckResizeImage(string imageFilePath)
        {
            System.Drawing.Image image = System.Drawing.Image.FromFile(imageFilePath);

            int resizeHeight = 340, resizeWidth = 620;
            int newHeight = 0, newWidth = 0;
            double aspectratio = 0;
            bool doResize = true;

            if (image.Width < resizeWidth)
            {
                aspectratio = (double)resizeWidth / (double)image.Width;
                newWidth = resizeWidth;
                newHeight = (int)(image.Height * aspectratio);

                if (newHeight < resizeHeight)
                {
                    aspectratio = (double)resizeHeight / (double)newHeight;
                    newHeight = resizeHeight;
                    newWidth = (int)(newWidth * aspectratio);
                }
            }
            else if (image.Height < resizeHeight)
            {
                aspectratio = (double)resizeHeight / (double)image.Height;
                newHeight = resizeHeight;
                newWidth = (int)(image.Width * aspectratio);

                if (newWidth < resizeWidth)
                {
                    aspectratio = (double)resizeWidth / (double)newWidth;
                    newWidth = resizeWidth;
                    newHeight = (int)(newHeight * aspectratio);
                }
            }
            else
                doResize = false;

            image.Dispose();
            return doResize;
        }

        private static System.Drawing.Image ResizeImage(string imageFilePath)
        {
            System.Drawing.Image image = System.Drawing.Image.FromFile(imageFilePath);

            int resizeHeight = 340, resizeWidth = 620;
            int newHeight = 0, newWidth = 0;
            double aspectratio = 0;
            bool doResize = true;

            if (image.Width < resizeWidth)
            {
                aspectratio = (double)resizeWidth / (double)image.Width;
                newWidth = resizeWidth;
                newHeight = (int)(image.Height * aspectratio);

                if (newHeight < resizeHeight)
                {
                    aspectratio = (double)resizeHeight / (double)newHeight;
                    newHeight = resizeHeight;
                    newWidth = (int)(newWidth * aspectratio);
                }
            }
            else if (image.Height < resizeHeight)
            {
                aspectratio = (double)resizeHeight / (double)image.Height;
                newHeight = resizeHeight;
                newWidth = (int)(image.Width * aspectratio);

                if (newWidth < resizeWidth)
                {
                    aspectratio = (double)resizeWidth / (double)newWidth;
                    newWidth = resizeWidth;
                    newHeight = (int)(newHeight * aspectratio);
                }
            }
            else
                doResize = false;

            System.Drawing.Image newImage = ResizeImageFinal(image, newWidth, newHeight);

            image.Dispose();
            return newImage;

        }

        private static System.Drawing.Image ResizeImage(string imageFilePath, int width, int height)
        {
            System.Drawing.Image image = System.Drawing.Image.FromFile(imageFilePath);

            int resizeHeight = height, resizeWidth = width;
            int newHeight = 0, newWidth = 0;
            double aspectratio = 0;
            bool doResize = true;

            if (image.Height < image.Width)
            {
                aspectratio = (double)resizeWidth / (double)image.Width;
                newWidth = resizeWidth;
                newHeight = (int)(image.Height * aspectratio);
            }
            else if (image.Width < image.Height)
            {
                aspectratio = (double)resizeHeight / (double)image.Height;
                newHeight = resizeHeight;
                newWidth = (int)(image.Width * aspectratio);
            }
            else
                doResize = false;

            System.Drawing.Image newImage = ResizeImageFinal(image, newWidth, newHeight);

            image.Dispose();
            return newImage;

        }

        static System.Drawing.Image ResizeImageFinal(System.Drawing.Image image, int desWidth, int desHeight)
        {
            int x, y, w, h;

            if (image.Height > image.Width)
            {
                w = (image.Width * desHeight) / image.Height;
                h = desHeight;
                x = (desWidth - w) / 2;
                y = 0;
            }
            else
            {
                w = desWidth;
                h = (image.Height * desWidth) / image.Width;
                x = 0;
                y = (desHeight - h) / 2;
            }

            var bmp = new System.Drawing.Bitmap(desWidth, desHeight);

            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bmp))
            {
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.DrawImage(image, x, y, w, h);
            }

            return bmp;
        }

        private static System.Drawing.Image ScaleImage(System.Drawing.Image image, int maxWidth)
        {
            var ratio = (double)maxWidth / image.Width;

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new System.Drawing.Bitmap(newWidth, newHeight);
            System.Drawing.Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
            return newImage;
        }

        public static bool resizeImage(System.Drawing.Image imageSource, string imageFilePath)
        {
            bool result = false;
            try
            {
                DateTime myTime = GeneralService.GetUTCDateTime();
                string filename = myTime.ToString("yyyyMMddHHmmss");
                System.Drawing.Image newThumbnailSmall = CreateThumbnail(imageSource, 60, 60, true);
                string newFilenameSmall = filename + "-small.jpg";
                newThumbnailSmall.Save(HttpContext.Current.Server.MapPath(imageFilePath + newFilenameSmall));

                System.Drawing.Image newThumbnailMedium = ScaleImage(imageSource, 200);
                string newFilenamewMedium = filename + "-medium.jpg";
                newThumbnailMedium.Save(HttpContext.Current.Server.MapPath(imageFilePath + newFilenamewMedium));

                result = true;
            }
            catch(Exception ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.ImageService);
            }

            return result;
        }
    }
}