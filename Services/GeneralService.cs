﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagement.Services
{
    public class GeneralService
    {
        public static DateTime GetUTCDateTime()
        {
            DateTime currentdatetime = DateTime.UtcNow.AddHours(8);
            return currentdatetime;
        }

        public static string GenerateGUID()
        {
            Random rnd = new Random();
            return rnd.Next(100000000, 999999999).ToString();
        }

        public static string GeneratePassword()
        {
            Random rnd = new Random();
            return rnd.Next(100000, 999999).ToString();
        }

        public static string FineTuneJSON(string item)
        {
            item = item.Replace("\\/Date(", "");
            item = item.Replace(")\\/", "");

            return item;
        }
    }
}