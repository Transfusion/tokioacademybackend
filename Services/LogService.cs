﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using EventManagement.Services;

namespace EventManagement.Services
{
    public class LogService
    {
        public static string DBConnection = "DBConnection";
        public static string EmailService = "EmailService";
        public static string ImageService = "ImageService";
        private static string logtime;

        public LogService()
        {
            string sYear = GeneralService.GetUTCDateTime().Year.ToString();
            string sMonth = GeneralService.GetUTCDateTime().Month.ToString("D2");
            string sDay = GeneralService.GetUTCDateTime().Day.ToString("D2");
            string sHour = GeneralService.GetUTCDateTime().Hour.ToString("D2");
            string sMinute = GeneralService.GetUTCDateTime().Minute.ToString("D2");
            string sSecond = GeneralService.GetUTCDateTime().Second.ToString("D2");
            logtime = sYear + sMonth + sDay + "-" + sHour + sMinute + sSecond;
        }

        private static void CreateLog(string pathname, string logmsg, string filename)
        {
            StreamWriter sw = new StreamWriter(pathname + filename + "-" + logtime + ".txt", true);
            sw.WriteLine(logmsg);
            sw.Flush();
            sw.Close();
        }

        public static void CreateErrorLog(string message, string filename)
        {
            try
            {
                LogService Err = new LogService();
                CreateLog(System.Web.HttpContext.Current.Server.MapPath("Logs/"), message, filename);
            }
            catch (Exception ex)
            {

            }
        }
    }
}