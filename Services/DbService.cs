﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using EventManagement.Models;
using System.Security.Cryptography;
using System.Text;

namespace EventManagement.Services
{
    public class DbService
    {
        #region user  
        public static int insertUser(UserModel item)
        {
            int result = 0;
            string cs = ConfigurationManager.ConnectionStrings["AgencyDB"].ConnectionString;

            MySqlConnection conn = null;
            MySqlDataReader rdr = null;

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
                //Console.WriteLine("MySQL version : {0}", conn.ServerVersion);
                //result= "MySQL version : " + conn.ServerVersion;

                string sql = "insert into rr_user values (null,@account,@name,@phone,@email,@password,@address,@pichead_url,@reg_time)";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("account", item.account);
                cmd.Parameters.AddWithValue("name", item.name);
                cmd.Parameters.AddWithValue("phone", item.phone);
                cmd.Parameters.AddWithValue("email", item.email);
                cmd.Parameters.AddWithValue("password", item.password);
                cmd.Parameters.AddWithValue("address", item.address);
                cmd.Parameters.AddWithValue("pichead_url", item.picHeadUrl);
                cmd.Parameters.AddWithValue("reg_time", item.regTime);
                cmd.ExecuteNonQuery();
                result = (int)cmd.LastInsertedId;
            }
            catch (MySqlException ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            catch (Exception ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

            return result;
        }

        public static int updateUser(UserModel item)
        {
            int result = 0;
            string cs = ConfigurationManager.ConnectionStrings["AgencyDB"].ConnectionString;

            MySqlConnection conn = null;
            MySqlDataReader rdr = null;

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
                //Console.WriteLine("MySQL version : {0}", conn.ServerVersion);
                //result= "MySQL version : " + conn.ServerVersion;

                string sql = "update rr_user set name=@name,phone=@phone,email=@email,password=@password,address=@address,pichead_url=@pichead_url where id=@id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("id", item.id);
                cmd.Parameters.AddWithValue("name", item.name);
                cmd.Parameters.AddWithValue("phone", item.phone);
                cmd.Parameters.AddWithValue("email", item.email);
                cmd.Parameters.AddWithValue("password", item.password);
                cmd.Parameters.AddWithValue("address", item.address);
                cmd.Parameters.AddWithValue("pichead_url", item.picHeadUrl);
                result = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            catch (Exception ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

            return result;
        }

        public static UserModel getUserById(int id)
        {
            UserModel item = new UserModel();
            string result = "";
            string cs = ConfigurationManager.ConnectionStrings["AgencyDB"].ConnectionString;

            MySqlConnection conn = null;
            MySqlDataReader rdr = null;

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
                //Console.WriteLine("MySQL version : {0}", conn.ServerVersion);
                //result= "MySQL version : " + conn.ServerVersion;

                string sql = "SELECT * FROM rr_user where id = @id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("id", id);
                rdr = cmd.ExecuteReader();

                if (rdr.Read())
                {
                    item.id = int.Parse(rdr["id"].ToString());
                    item.account = rdr["account"].ToString();
                    item.name = rdr["name"].ToString();
                    item.phone = rdr["phone"].ToString();
                    item.email = rdr["email"].ToString();
                    item.password = rdr["password"].ToString();
                    item.address = rdr["address"].ToString();
                    item.picHeadUrl = rdr["pichead_url"].ToString();
                    item.regTime = DateTime.Parse(rdr["reg_time"].ToString());
                }
            }
            catch (MySqlException ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            catch (Exception ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

            return item;
        }

        public static UserModel getUserByAccount(string account)
        {
            UserModel item = new UserModel();
            string result = "";
            string cs = ConfigurationManager.ConnectionStrings["AgencyDB"].ConnectionString;

            MySqlConnection conn = null;
            MySqlDataReader rdr = null;

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
                //Console.WriteLine("MySQL version : {0}", conn.ServerVersion);
                //result= "MySQL version : " + conn.ServerVersion;

                string sql = "SELECT * FROM rr_user where account = @account";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("account", account);
                rdr = cmd.ExecuteReader();

                if (rdr.Read())
                {
                    item.id = int.Parse(rdr["id"].ToString());
                    item.account = rdr["account"].ToString();
                    item.name = rdr["name"].ToString();
                    item.phone = rdr["phone"].ToString();
                    item.email = rdr["email"].ToString();
                    item.password = rdr["password"].ToString();
                    item.address = rdr["address"].ToString();
                    item.picHeadUrl = rdr["pichead_url"].ToString();
                    item.regTime = DateTime.Parse(rdr["reg_time"].ToString());
                }
            }
            catch (MySqlException ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            catch (Exception ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

            return item;
        }

        public static UserModel getUserByEmail(string email)
        {
            UserModel item = new UserModel();
            string result = "";
            string cs = ConfigurationManager.ConnectionStrings["AgencyDB"].ConnectionString;

            MySqlConnection conn = null;
            MySqlDataReader rdr = null;

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
                //Console.WriteLine("MySQL version : {0}", conn.ServerVersion);
                //result= "MySQL version : " + conn.ServerVersion;

                string sql = "SELECT * FROM rr_user where email = @email";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("email", email);
                rdr = cmd.ExecuteReader();

                if (rdr.Read())
                {
                    item.id = int.Parse(rdr["id"].ToString());
                    item.account = rdr["account"].ToString();
                    item.name = rdr["name"].ToString();
                    item.phone = rdr["phone"].ToString();
                    item.email = rdr["email"].ToString();
                    item.password = rdr["password"].ToString();
                    item.address = rdr["address"].ToString();
                    item.picHeadUrl = rdr["pichead_url"].ToString();
                    item.regTime = DateTime.Parse(rdr["reg_time"].ToString());
                }
            }
            catch (MySqlException ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            catch (Exception ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

            return item;
        }
        #endregion

        #region event  
        public static List<EventModel> getEventList()
        {
            List<EventModel> list = new List<EventModel>();
            string result = "";
            string cs = ConfigurationManager.ConnectionStrings["AgencyDB"].ConnectionString;

            MySqlConnection conn = null;
            MySqlDataReader rdr = null;

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
                //Console.WriteLine("MySQL version : {0}", conn.ServerVersion);
                //result= "MySQL version : " + conn.ServerVersion;

                string sql = "SELECT * FROM rr_event order by id desc";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    EventModel item = new EventModel();
                    item.id = int.Parse(rdr["id"].ToString());
                    item.name = rdr["name"].ToString();
                    item.description = rdr["description"].ToString();
                    item.startTime = rdr["start_date"].ToString() != "" ? DateTime.Parse(rdr["start_date"].ToString()) : DateTime.MinValue;
                    item.endTime = rdr["end_date"].ToString() != "" ? DateTime.Parse(rdr["end_date"].ToString()) : DateTime.MinValue;
                    item.displayStartTime = rdr["display_start_date"].ToString() != "" ? DateTime.Parse(rdr["display_start_date"].ToString()) : DateTime.MinValue;
                    item.displayEndTime = rdr["display_end_date"].ToString() != "" ? DateTime.Parse(rdr["display_end_date"].ToString()) : DateTime.MinValue;
                    item.picHeadUrl = rdr["pichead_url"].ToString();
                    item.eventType = rdr["event_type"].ToString();
                    item.status = (EventStatus)(int.Parse(rdr["status"].ToString()));
                    item.createdDate = rdr["created_date"].ToString() != "" ? DateTime.Parse(rdr["created_date"].ToString()) : DateTime.MinValue;
                    item.createdBy = rdr["created_by"].ToString();
                    item.modifiedDate = rdr["modified_date"].ToString() != "" ? DateTime.Parse(rdr["modified_date"].ToString()) : DateTime.MinValue;
                    item.modifiedBy = rdr["modified_by"].ToString();
                    list.Add(item);
                }
            }
            catch (MySqlException ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            catch (Exception ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

            return list;
        }

        public static EventModel getEventById(int id)
        {
            EventModel item = new EventModel();
            string cs = ConfigurationManager.ConnectionStrings["AgencyDB"].ConnectionString;

            MySqlConnection conn = null;
            MySqlDataReader rdr = null;

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
                //Console.WriteLine("MySQL version : {0}", conn.ServerVersion);
                //result= "MySQL version : " + conn.ServerVersion;

                string sql = "SELECT * FROM rr_event where id = @id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("id", id);
                rdr = cmd.ExecuteReader();

                if (rdr.Read())
                {
                    item.id = int.Parse(rdr["id"].ToString());
                    item.name = rdr["name"].ToString();
                    item.description = rdr["description"].ToString();
                    item.startTime = DateTime.Parse(rdr["start_date"].ToString());
                    item.endTime = DateTime.Parse(rdr["end_date"].ToString());
                    item.displayStartTime = DateTime.Parse(rdr["display_start_date"].ToString());
                    item.displayEndTime = DateTime.Parse(rdr["display_end_date"].ToString());
                    item.picHeadUrl = rdr["pichead_url"].ToString();
                    item.eventType = rdr["event_type"].ToString();
                    item.status = (EventStatus)(int.Parse(rdr["status"].ToString()));
                    item.createdDate = DateTime.Parse(rdr["created_date"].ToString());
                    item.createdBy = rdr["created_by"].ToString();
                    item.modifiedDate = DateTime.Parse(rdr["modified_date"].ToString());
                    item.modifiedBy = rdr["modified_by"].ToString();
                }
            }
            catch (MySqlException ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            catch (Exception ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

            return item;
        }

        public static bool CreateEvent(String ActionerID, EventModel newEvent, out String EventID)
        {
            bool result = false;
            string cs = ConfigurationManager.ConnectionStrings["AgencyDB"].ConnectionString;

            MySqlConnection conn = null;
            MySqlDataReader rdr = null;
            EventID = "";
            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
                //Console.WriteLine("MySQL version : {0}", conn.ServerVersion);
                //result= "MySQL version : " + conn.ServerVersion;
                String EventGuid = Guid.NewGuid().ToString();
                string sql = @"INSERT INTO rr_event
                                VALUES
                                (@id,
                                @name,
                                @description,
                                @start_date,
                                @end_date,
                                @display_start_date,
                                @display_end_date,
                                @pichead_url,
                                @event_type,
                                @status,
                                @created_date,
                                @created_by,
                                @modified_date,
                                @modified_by);";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("id", EventGuid);
                cmd.Parameters.AddWithValue("name", newEvent.name);
                cmd.Parameters.AddWithValue("description", newEvent.description);
                cmd.Parameters.AddWithValue("start_date", newEvent.startTime);
                cmd.Parameters.AddWithValue("end_date", newEvent.endTime);
                cmd.Parameters.AddWithValue("display_start_date", newEvent.displayStartTime);
                cmd.Parameters.AddWithValue("display_end_date", newEvent.displayEndTime);
                cmd.Parameters.AddWithValue("pichead_url", newEvent.picHeadUrl);
                cmd.Parameters.AddWithValue("event_type", newEvent.eventType);
                cmd.Parameters.AddWithValue("status", EventStatus.Active);
                cmd.Parameters.AddWithValue("created_date", DateTime.Now);
                cmd.Parameters.AddWithValue("created_by", ActionerID);
                cmd.Parameters.AddWithValue("modified_date", DateTime.Now);
                cmd.Parameters.AddWithValue("modified_by", ActionerID);
                cmd.ExecuteNonQuery();
                EventID = cmd.LastInsertedId.ToString();

                result = true;
            }
            
            catch (Exception ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

            return result;
           
        }

        public static bool EditEvent(String ActionerID, EventModel newEvent, out String ErrMsg)
        {
            ErrMsg = "";
            bool result = false;
            string cs = ConfigurationManager.ConnectionStrings["AgencyDB"].ConnectionString;

            MySqlConnection conn = null;
           
            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
                //Console.WriteLine("MySQL version : {0}", conn.ServerVersion);
                //result= "MySQL version : " + conn.ServerVersion;
             
                string sql = @"UPDATE rr_event SET
                                name = @name,
                                description = @description,
                                start_date = @start_date,
                                end_date = @end_date,
                                display_start_date = @display_start_date,
                                display_end_date = @display_end_date,
                                pichead_url = @pichead_url,
                                event_type = @event_type,
                                status = @status,
                                modified_date = @modified_date,
                                modified_by = @modified_by
                                WHERE id = @id";

                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("id", newEvent.id);
                cmd.Parameters.AddWithValue("name", newEvent.name);
                cmd.Parameters.AddWithValue("description", newEvent.description);
                cmd.Parameters.AddWithValue("start_date", newEvent.startTime);
                cmd.Parameters.AddWithValue("end_date", newEvent.endTime);
                cmd.Parameters.AddWithValue("display_start_date", newEvent.displayStartTime);
                cmd.Parameters.AddWithValue("display_end_date", newEvent.displayEndTime);
                cmd.Parameters.AddWithValue("pichead_url", newEvent.picHeadUrl);
                cmd.Parameters.AddWithValue("event_type", newEvent.eventType);
                cmd.Parameters.AddWithValue("status", newEvent.status);
                cmd.Parameters.AddWithValue("modified_date", DateTime.Now);
                cmd.Parameters.AddWithValue("modified_by", ActionerID);
                cmd.ExecuteNonQuery();

                result = true;
            }

            catch (Exception ex)
            {
                ErrMsg = "Error : " + ex.ToString();
                //LogService.CreateErrorLog(ErrMsg, LogService.DBConnection);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

            return result;

        }
    

        public static List<EventContent> getEventContentById(int id)
        {
            List<EventContent> EventContent = new List<EventContent>();
            string cs = ConfigurationManager.ConnectionStrings["AgencyDB"].ConnectionString;

            MySqlConnection conn = null;
            MySqlDataReader rdr = null;

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
                //Console.WriteLine("MySQL version : {0}", conn.ServerVersion);
                //result= "MySQL version : " + conn.ServerVersion;

                string sql = "SELECT * FROM rr_event_content where event_id = @id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("id", id);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    EventContent item = new EventContent();
                    item.id = int.Parse(rdr["id"].ToString());
                    item.contentName = rdr["content_name"].ToString();
                    item.contentLogo = rdr["content_logo"].ToString();
                    item.contentType = rdr["content_type"].ToString();
                    item.contentVisibility = rdr["content_visibility"].ToString();
                    item.remark = rdr["remark"].ToString();
                    item.createdDate = DateTime.Parse(rdr["created_date"].ToString());
                    item.createdBy = rdr["created_by"].ToString();
                    item.modifiedDate = DateTime.Parse(rdr["modified_date"].ToString());
                    item.modifiedBy = rdr["modified_by"].ToString();
                    EventContent.Add(item);
                }
            }
            catch (MySqlException ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            catch (Exception ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

            return EventContent;
        }

        public static EventContent GetEventContent(int id)
        {
            EventContent item = new EventContent();
            string cs = ConfigurationManager.ConnectionStrings["AgencyDB"].ConnectionString;

            MySqlConnection conn = null;
            MySqlDataReader rdr = null;

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
                //Console.WriteLine("MySQL version : {0}", conn.ServerVersion);
                //result= "MySQL version : " + conn.ServerVersion;

                string sql = "SELECT * FROM rr_event_content where id = @id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("id", id);
                rdr = cmd.ExecuteReader();

                if (rdr.Read())
                {
                    
                    item.id = int.Parse(rdr["id"].ToString());
                    item.eventId = int.Parse(rdr["event_id"].ToString());
                    item.contentName = rdr["content_name"].ToString();
                    item.contentLogo = rdr["content_logo"].ToString();
                    item.contentType = rdr["content_type"].ToString();
                    item.contentVisibility = rdr["content_visibility"].ToString();
                    item.remark = rdr["remark"].ToString();
                    item.createdDate = DateTime.Parse(rdr["created_date"].ToString());
                    item.createdBy = rdr["created_by"].ToString();
                    item.modifiedDate = DateTime.Parse(rdr["modified_date"].ToString());
                    item.modifiedBy = rdr["modified_by"].ToString();
                 
                }
            }
            catch (MySqlException ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            catch (Exception ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

            return item;
        }

        public static bool CreateEventContent(String ActionerID, EventContent newEventContent)
        {
            bool result = false;
            string cs = ConfigurationManager.ConnectionStrings["AgencyDB"].ConnectionString;

            MySqlConnection conn = null;
            MySqlDataReader rdr = null;
          
            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
                //Console.WriteLine("MySQL version : {0}", conn.ServerVersion);
                //result= "MySQL version : " + conn.ServerVersion;
                String EventGuid = Guid.NewGuid().ToString();
                string sql = @"INSERT INTO rr_event_content
                                VALUES
                                (@id,
                                @eventid,
                                @name,
                                @logo,
                                @type,
                                @visibility,
                                @remark,
                                @created_date,
                                @created_by,
                                @modified_date,
                                @modified_by);";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("id", EventGuid);
                cmd.Parameters.AddWithValue("eventid", newEventContent.eventId);
                cmd.Parameters.AddWithValue("name", newEventContent.contentName);
                cmd.Parameters.AddWithValue("logo", newEventContent.contentLogo);
                cmd.Parameters.AddWithValue("type", ((ContentType)int.Parse(newEventContent.contentType)).ToString());
                cmd.Parameters.AddWithValue("visibility", ((ContentVisibility)int.Parse(newEventContent.contentVisibility)).ToString());
                //cmd.Parameters.AddWithValue("type", newEventContent.contentType);
                //cmd.Parameters.AddWithValue("visibility", newEventContent.contentVisibility);
                cmd.Parameters.AddWithValue("remark", newEventContent.remark);
                cmd.Parameters.AddWithValue("created_date", DateTime.Now);
                cmd.Parameters.AddWithValue("created_by", ActionerID);
                cmd.Parameters.AddWithValue("modified_date", DateTime.Now);
                cmd.Parameters.AddWithValue("modified_by", ActionerID);
                cmd.ExecuteNonQuery();
               
                result = true;
            }

            catch (Exception ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

            return result;

        }
        
        #endregion

        #region Admin
        public static bool Login(String UserName, String Password, out String ErrorMessage)
        {
            ErrorMessage = "";
            bool result = false;
            string cs = ConfigurationManager.ConnectionStrings["AgencyDB"].ConnectionString;

            MySqlConnection conn = null;
            MySqlDataReader rdr = null;

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
                //Console.WriteLine("MySQL version : {0}", conn.ServerVersion);
                //result= "MySQL version : " + conn.ServerVersion;
                string sql = "SELECT * FROM rr_admin where rr_username = @UserName";
                LoginModel checkLogin = new LoginModel();
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("UserName", UserName);
                rdr = cmd.ExecuteReader();
                if (!rdr.Read())
                {
                    throw new Exception("Invalid Username!");
                }
                else
                {
                    checkLogin.UserID = rdr["rr_username"].ToString();
                    checkLogin.Password = rdr["rr_password"].ToString();
                    checkLogin.Status = (LoginStatus)(int.Parse(rdr["rr_status"].ToString()));
                }

                String PasswordHash = GenerateHash(Password);

                if (PasswordHash != checkLogin.Password)
                {
                    throw new Exception("Invalid Password!");
                }

                if (checkLogin.Status != LoginStatus.Active)
                {
                    throw new Exception("Account Status : " + checkLogin.Status.ToString());
                }

                if (conn != null)
                {
                    conn.Close();
                }

                conn.Open();
                string sqlUpdate = @"Update rr_admin set rr_lastlogindate = NOW() where rr_username = @UserName";
                cmd = new MySqlCommand(sqlUpdate, conn);
                cmd.Parameters.AddWithValue("UserName", UserName);
                cmd.ExecuteNonQuery();

                result = true;
            }

            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                //LogService.CreateErrorLog(ex.ToString(), LogService.DBConnection);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

            return result;
        }


        public static bool CreateAdmin(String ActionerID, String UserName, String Password, out String ErrorMessage)
        {
            ErrorMessage = "";
            bool result = false;
            string cs = ConfigurationManager.ConnectionStrings["AgencyDB"].ConnectionString;

            MySqlConnection conn = null;
            
            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
                //Console.WriteLine("MySQL version : {0}", conn.ServerVersion);
                //result= "MySQL version : " + conn.ServerVersion;

             
                String PasswordHash = GenerateHash(Password);
                string sql = @"INSERT INTO rr_admin
                            VALUES
                            (@id,
                            @rr_username,
                            @rr_password,
                            @rr_status,
                            @rr_lastlogindate,
                            @rr_createddate,
                            @rr_createdby,
                            @rr_modifieddate,
                            @rr_modifiedby);";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("id", Guid.NewGuid());
                cmd.Parameters.AddWithValue("rr_username", UserName);
                cmd.Parameters.AddWithValue("rr_password", PasswordHash);
                cmd.Parameters.AddWithValue("rr_status", LoginStatus.Active);
                cmd.Parameters.AddWithValue("rr_lastlogindate", DateTime.MinValue);
                cmd.Parameters.AddWithValue("rr_createddate", DateTime.Now);
                cmd.Parameters.AddWithValue("rr_createdby", ActionerID);
                cmd.Parameters.AddWithValue("rr_modifieddate", DateTime.Now);
                cmd.Parameters.AddWithValue("rr_modifiedby", ActionerID);
                cmd.ExecuteNonQuery();
              
                result = true;
            }

            catch (Exception ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.DBConnection);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

            return result;
        }

        private static String GenerateHash(String Password)
        {
            var md5 = new MD5CryptoServiceProvider();
            var md5data = md5.ComputeHash(Encoding.ASCII.GetBytes(Password));

            return Encoding.ASCII.GetString(md5data);
        }

        #endregion
    }
}