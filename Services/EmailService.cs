﻿using EventManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace EventManagement.Services
{
    public class EmailService
    {
        private static void sendMail(MailMessage msg)
        {
            string username = "mrbing1988@gmail.com";  //email address or domain user for exchange authentication
            string password = "bing1988";  //password
            SmtpClient mClient = new SmtpClient();
            mClient.Host = "smtp.gmail.com";
            mClient.Credentials = new NetworkCredential(username, password);
            mClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            mClient.Port = 587;
            mClient.EnableSsl = true;
            mClient.Timeout = 100000;
            mClient.Send(msg);
        }

        public static void sendResetPassword(UserModel user)
        {
            try {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress("app-support@tokiomarine.com.my");
                msg.To.Add(user.email);
                msg.Subject = "Reset Password - Tokio Marine Event App";
                msg.IsBodyHtml = true;
                msg.BodyEncoding = Encoding.ASCII;
                msg.Body = "Hi " + user.name + ",";
                msg.Body += "<br><br>Email: " + user.email;
                msg.Body += "<br>Password: " + GeneralService.GeneratePassword();
                sendMail(msg);
            }
            catch(Exception ex)
            {
                string message = "Error : " + ex.ToString();
                LogService.CreateErrorLog(message, LogService.EmailService);
            }
        }
    }
}