﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagement.Models
{
    public class EventModel
    {
        private long Id = 0;
        public long id
        {
            get { return Id; }
            set { Id = value; }
        }

        private string Name = string.Empty;
        public string name
        {
            get { return Name; }
            set { Name = value; }
        }

        private string Description = string.Empty;
        public string description
        {
            get { return Description; }
            set { Description = value; }
        }

        private DateTime start_time = new DateTime(1900, 1, 1);
        public DateTime startTime
        {
            get { return start_time; }
            set { start_time = value; }
        }

        private DateTime end_time = new DateTime(1900, 1, 1);
        public DateTime endTime
        {
            get { return end_time; }
            set { end_time = value; }
        }

        private DateTime display_start_time = new DateTime(1900, 1, 1);
        public DateTime displayStartTime
        {
            get { return display_start_time; }
            set { display_start_time = value; }
        }

        private DateTime display_end_time = new DateTime(1900, 1, 1);
        public DateTime displayEndTime
        {
            get { return display_end_time; }
            set { display_end_time = value; }
        }

        private string picheal_url = string.Empty;
        public string picHeadUrl
        {
            get { return picheal_url; }
            set { picheal_url = value; }
        }

        private string event_type = string.Empty;
        public string eventType
        {
            get { return event_type; }
            set { event_type = value; }
        }

        private string Status = string.Empty;
        public string status
        {
            get { return Status; }
            set { Status = value; }
        }

        private DateTime created_date = new DateTime(1900, 1, 1);
        public DateTime createdDate
        {
            get { return created_date; }
            set { created_date = value; }
        }

        private string created_by = string.Empty;
        public string createdBy
        {
            get { return created_by; }
            set { created_by = value; }
        }

        private DateTime modified_date = new DateTime(1900, 1, 1);
        public DateTime modifiedDate
        {
            get { return modified_date; }
            set { modified_date = value; }
        }

        private string modified_by = string.Empty;
        public string modifiedBy
        {
            get { return modified_by; }
            set { modified_by = value; }
        }
    }
}