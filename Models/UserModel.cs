﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagement.Models
{
    public class UserModel
    {
        private long Id = 0;
        public long id
        {
            get { return Id; }
            set { Id = value; }
        }

        private string Account = string.Empty;
        public string account
        {
            get { return Account; }
            set { Account = value; }
        }

        private string Name = string.Empty;
        public string name
        {
            get { return Name; }
            set { Name = value; }
        }

        private string Phone = string.Empty;
        public string phone
        {
            get { return Phone; }
            set { Phone = value; }
        }

        private string Email = string.Empty;
        public string email
        {
            get { return Email; }
            set { Email = value; }
        }

        private string Password = string.Empty;
        public string password
        {
            get { return Password; }
            set { Password = value; }
        }

        private string Address = string.Empty;
        public string address
        {
            get { return Address; }
            set { Address = value; }
        }

        private string picheal_url = string.Empty;
        public string picHeadUrl
        {
            get { return picheal_url; }
            set { picheal_url = value; }
        }

        private DateTime reg_time = new DateTime(1900, 1, 1);
        public DateTime regTime
        {
            get { return reg_time; }
            set { reg_time = value; }
        }
    }
}