﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagement.Models
{
    public class EventContent
    {
        private long Id = 0;
        public long id
        {
            get { return Id; }
            set { Id = value; }
        }

        private long event_id = 0;
        public long eventId
        {
            get { return event_id; }
            set { event_id = value; }
        }

        private string content_name = string.Empty;
        public string contentName
        {
            get { return content_name; }
            set { content_name = value; }
        }

        private string content_logo = string.Empty;
        public string contentLogo
        {
            get { return content_logo; }
            set { content_logo = value; }
        }

        private string content_type = string.Empty;
        public string contentType
        {
            get { return content_type; }
            set { content_type = value; }
        }

        private string content_visibility = string.Empty;
        public string contentVisibility
        {
            get { return content_visibility; }
            set { content_visibility = value; }
        }

        private string Remark = string.Empty;
        public string remark
        {
            get { return Remark; }
            set { Remark = value; }
        }

        private DateTime created_date = new DateTime(1900, 1, 1);
        public DateTime createdDate
        {
            get { return created_date; }
            set { created_date = value; }
        }

        private string created_by = string.Empty;
        public string createdBy
        {
            get { return created_by; }
            set { created_by = value; }
        }

        private DateTime modified_date = new DateTime(1900, 1, 1);
        public DateTime modifiedDate
        {
            get { return modified_date; }
            set { modified_date = value; }
        }

        private string modified_by = string.Empty;
        public string modifiedBy
        {
            get { return modified_by; }
            set { modified_by = value; }
        }
    }
}