﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagement.Models
{
    public class EventParticipant
    {
        private long Id = 0;
        public long id
        {
            get { return Id; }
            set { Id = value; }
        }

        private long event_id = 0;
        public long eventId
        {
            get { return event_id; }
            set { event_id = value; }
        }

        private long account_id = 0;
        public long accountId
        {
            get { return account_id; }
            set { account_id = value; }
        }

        private int is_attend = 0;
        public int isAttend
        {
            get { return is_attend; }
            set { is_attend = value; }
        }

        private DateTime attend_date = new DateTime(1900, 1, 1);
        public DateTime attendDate
        {
            get { return attend_date; }
            set { attend_date = value; }
        }

        private string Remark = string.Empty;
        public string remark
        {
            get { return Remark; }
            set { Remark = value; }
        }

        private DateTime created_date = new DateTime(1900, 1, 1);
        public DateTime createdDate
        {
            get { return created_date; }
            set { created_date = value; }
        }

        private string created_by = string.Empty;
        public string createdBy
        {
            get { return created_by; }
            set { created_by = value; }
        }

        private DateTime modified_date = new DateTime(1900, 1, 1);
        public DateTime modifiedDate
        {
            get { return modified_date; }
            set { modified_date = value; }
        }

        private string modified_by = string.Empty;
        public string modifiedBy
        {
            get { return modified_by; }
            set { modified_by = value; }
        }
    }
}